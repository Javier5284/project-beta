import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sales_project.settings")
django.setup()

# Import models from sales_rest, here.
from sales_rest.models import AutomobileVO

# from sales_rest.models import Something


def get_automobiles():
    response = requests.get("http://project-beta-inventory-api-1:8000/api/automobiles")
    content = json.loads(response.content)
    for automobile in content["autos"]:
        vin = automobile["vin"]
        sold = automobile["sold"]

        try:
            automobile_vo = AutomobileVO.objects.get(vin=vin)
            automobile_vo.sold = sold
            automobile_vo.save()
        except AutomobileVO.DoesNotExist:
            try:
                AutomobileVO.objects.create(vin=vin, sold=sold)
            except Exception as e:
                if 'duplicate key value violates unique constraint' in str(e):
                    print(f"Duplicate key violation for vin: {vin}")
                else:
                    print(f"An error occurred: {str(e)}")


def poll(repeat=True):
    while True:
        try:
            get_automobiles()
        except Exception as e:
            print(e, file=sys.stderr)

        if not repeat:
            break

        time.sleep(60)


if __name__ == "__main__":
    poll()
