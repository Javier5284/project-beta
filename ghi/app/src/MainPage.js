import React from 'react';
import "./index.css";


function MainPage() {
  return (
    <div>
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">CarCar</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            The premiere solution for automobile dealership management!
          </p>
        </div>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-lg-6 mb-5">
            <div className="card mb-4 card-height">
            <img src="https://www.carmax.com/home/images/home/hero/lycg-tablet.jpg" class="card-img-top" alt="..."></img>
              <div className="card-body">
                <h5 className="card-title">Customers</h5>
                <p className="card-text">
                  Add a customer to the database, or search for an existing customer.
                </p>
              </div>
            </div>
          </div>
          <div className="col-lg-6 mb-5">
            <div className="card mb-4 card-height">
              <img src="https://www.carmax.com/home/images/why-carmax/our-people/no-haggling-or-upselling/no-haggling-or-upselling.png" class="card-img-top" alt="..."></img>
              <div className="card-body">
                <h5 className="card-title">Automobiles</h5>
                <p className="card-text">
                 To add an automobile to the database, you must first add a manufacturer and a model. Included are the ability to search for existing automobiles, models, and manufacturers.
                </p>
              </div>
            </div>
          </div>
          <div className="col-lg-6">
            <div className="card mb-4 card-height">
              <img src="https://www.carmax.com/home/images/service/guaranteed-repairs.png" class="card-img-top" alt="..."></img>
              <div className="card-body">
                <h5 className="card-title">Sales</h5>
                <p className="card-text">
                  Inside the sales dropdwon you can find the ability to create a sale, search for existing sales, and view sales reports.
                </p>
              </div>
            </div>
          </div>
          <div className="col-lg-6">
            <div className="card mb-4 card-height">
              <img src="https://www.carmax.com/home/images/service/servicing-options-color-block.jpg" class="card-img-top" alt="..."></img>
              <div className="card-body">
                <h5 className="card-title">Service</h5>
                <p className="card-text">
                  The Service dropdown includes the ability to create a service appointment, search for existing service appointments, view service history, add a technician and view all your technicians.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default MainPage;
