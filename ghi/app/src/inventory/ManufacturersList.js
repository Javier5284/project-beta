import React, { useState, useEffect } from "react";

function ManufacturersList() {
  const [manufacturers, setManufacturers] = useState([]);

  const fetchData = async () => {
    const url = "http://localhost:8100/api/manufacturers/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <>
    <h1>List of Manufacturers</h1>
    <table className="table table-striped">
        <thead>
            <tr>
                <th>Manufacturer</th>
            </tr>
        </thead>
        <tbody>
            {manufacturers.map((manufacturer) => {
                return (
                    <tr key={manufacturer.id}>
                        <td>{manufacturer.name}</td>
                    </tr>
                );
            })}
        </tbody>
    </table>
    <a href= "http://localhost:3000/manufacturers/new" role="button" className="btn btn-success">+ Add a new Manufacturer</a>
    </>
  )

}

export default ManufacturersList;
