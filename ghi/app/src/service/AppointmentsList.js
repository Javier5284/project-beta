import React, { useState, useEffect } from "react";

function AppointmentsList() {
  const [appointments, setAppointments] = useState([]);

  const fetchData = async () => {
    const url = "http://localhost:8080/api/appointments/";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments);
    }
  };
  useEffect(() => {
    fetchData();
  }, []);

  const handleFinished = async (id) => {
    const finishedUrl = `http://localhost:8080/api/appointments/${id}/finish`;
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify({ status: "Finished" }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(finishedUrl, fetchConfig);
    if (response.ok) {
      const updatedAppointments = appointments.map(appointment => {
        if (appointment.id === id) {
          return { ...appointment, status: "Finished" };
        }
        return appointment;
      });
      setAppointments(updatedAppointments);
    }
  };

  const handleCanceled = async (id) => {
    const canceledUrl = `http://localhost:8080/api/appointments/${id}/cancel`;
    const fetchConfig = {
      method: "PUT",
      body: JSON.stringify({ status: "Canceled" }),
      headers: {
        "Content-Type": "application/json",
      },
    };

    const response = await fetch(canceledUrl, fetchConfig);
    if (response.ok) {
      const updatedAppointments = appointments.map(appointment => {
        if (appointment.id === id) {
          return { ...appointment, status: "canceled" };
        }
        return appointment;
      });
      setAppointments(updatedAppointments);
    }
  };

  return (
    <>
      <h1 style={{ marginTop: "20px" }}>Service Appointments</h1>
      <table className="table table-striped">
          <thead>
              <tr>
                  <th>VIN</th>
                  <th>VIP</th>
                  <th>customer</th>
                  <th>Date</th>
                  <th>Time</th>
                  <th>Technician</th>
                  <th>Reason</th>
                  <th>Change Status</th>
              </tr>
          </thead>
          <tbody>
              {appointments.map((appointment) => {
                const date = new Date(appointment.date_time).toLocaleDateString();
                const time = new Date(appointment.date_time).toLocaleTimeString(
                  [],
                  { timeStyle: "short" }
                );

                let visible = "";
                if (appointment.status !== "active") {
                  visible = "d-none";
                }
                  return (
                      <tr className={visible} key={appointment.id}>
                          <td>{appointment.vin}</td>
                          <td>{appointment.vip_status ? "Yes" : "No"}</td>
                          <td>{appointment.customer}</td>
                          <td>{date}</td>
                          <td>{time}</td>
                          <td>{appointment.technician.employee_id}</td>
                          <td>{appointment.reason}</td>
                          <td>
                            <button onClick={() => handleCanceled(appointment.id)} className="btn btn-danger">
                              Cancel
                            </button>
                            <button onClick={() => handleFinished(appointment.id)} className="btn btn-success">
                              Finish
                            </button>
                          </td>
                      </tr>
                  );
              })}
          </tbody>
      </table>
      <a href= "http://localhost:3000/appointments/new" role="button" className="btn btn-success">+ Make an appointment</a>
    </>
  );

}
  export default AppointmentsList;
