import React, { useState, useEffect } from "react";

function ServiceHistory() {
    const [appointments, setAppointments] = useState([]);
    const [filterValue, setFilterValue] = useState("");
    const [filterType, setFilterType] = useState("");

    const fetchData = async () => {
        const url = "http://localhost:8080/api/appointments/";
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    const getStatusColor = (status) => {
        if (status === "finished") {
            return "limegreen";
        } else if (status === "canceled") {
            return "red";
        } else {
            return "blue";
        }
    };

    const handleFilterChange = (event) => {
        setFilterValue(event.target.value)
    }

    const handleFilterTypeChange = (event) => {
        setFilterType(event.target.value)
    }

    const getFilteredVins = () => {
        return appointments.filter(a => String(a[filterType]).toLowerCase().includes(filterValue.toLowerCase()))
    }

    return (
        <>
        <h1 style={{ marginTop: "20px" }}>Service History</h1>

        <select onChange={handleFilterTypeChange} placeholder='Search'>
            <option value="vin">Vin</option>
            <option value="customer">Customer</option>
        </select>

        <input onChange={handleFilterChange} placeholder='Search'/>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>is VIP?</th>
                    <th>customer</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Technician</th>
                    <th>Reason</th>
                    <th>status</th>
                </tr>
            </thead>
            <tbody>
                {getFilteredVins().map((appointment) => {
                    const date = new Date(appointment.date_time).toLocaleDateString();
                    const time = new Date(appointment.date_time).toLocaleTimeString( [] , { timeStyle: "short" });
                    return (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.vip_status ? "Yes" : "No"}</td>
                            <td>{appointment.customer}</td>
                            <td>{date}</td>
                            <td>{time}</td>
                            <td>{appointment.technician.first_name} {appointment.technician.last_name}</td>
                            <td>{appointment.reason}</td>
                            <td style={{ color: getStatusColor(appointment.status) }}>{appointment.status}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        </>
    );
}
export default ServiceHistory;
