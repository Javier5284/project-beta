import React, { useEffect, useState } from "react";


const SalesPersonForm = () => {
    const [formData, setFormData] = useState({
        first_name: "",
        last_name: "",
        employee_id: "",
});

    const handleSubmit = async (e) => {
        e.preventDefault();
        const salesPersonUrl = "http://localhost:8090/api/salespeople/";
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json",
        },
    };

    const response = await fetch(salesPersonUrl, fetchConfig);
    if (response.ok) {
        setFormData({
            first_name: "",
            last_name: "",
            employee_id: "",
        });
        alert("Salesperson created successfully!");
    }
};

    const handleChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;
        setFormData({
             ...formData,
             [inputName]: value
        });
    };

    return (
        <form onSubmit={handleSubmit}>
            <div className ="form-group">
                <label htmlFor="first_name">First Name</label>
                <input
                    value={formData.first_name}
                    onChange={handleChange}
                    required
                    type="text"
                    className="form-control"
                    id="first_name"
                    name="first_name"
                    placeholder="First Name"
                />
            </div>
            <div className ="form-group">
                <label htmlFor="last_name">Last Name</label>
                <input
                    value={formData.last_name}
                    onChange={handleChange}
                    required
                    type="text"
                    className="form-control"
                    id="last_name"
                    name="last_name"
                    placeholder="Last Name"
                    />
            </div>
            <div className ="form-group">
                <label htmlFor="employee_id">Employee ID</label>
                <input
                    value={formData.employee_id}
                    onChange={handleChange}
                    required
                    type="text"
                    className="form-control"
                    id="employee_id"
                    name="employee_id"
                    placeholder="Employee ID"
                />
            </div>
            <button type="submit" className="btn btn-primary">
                Create
            </button>
            </form>
    );
};


export default SalesPersonForm;
