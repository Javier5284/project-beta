import React, { useState, useEffect } from "react";


const ListSalesPeople = () => {
    const [salespeople, setSalesPeople] = useState([]);

    const fetchSalesPeople = async () => {
        const url = "http://localhost:8090/api/salespeople/";
        const response = await fetch(url);
        const data = await response.json();
        if (response.ok) {
            setSalesPeople(data.salespeople);
        }
    };

    useEffect(() => {
        fetchSalesPeople();
    }   , []);

    return (
        <div className="container">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Employee Id</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map((salesperson) => (
                        <tr key={salesperson["Django ID"]}>
                            <td>{salesperson.employee_id}</td>
                            <td>{salesperson.first_name}</td>
                            <td>{salesperson.last_name}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default ListSalesPeople;
