import React, { useState, useEffect } from "react";

const ListAllSales = () => {
    const [sales, setSales] = useState([]);

    const fetchSales = async () => {
        const url = "http://localhost:8090/api/sales/";
        const response = await fetch(url);
        const data = await response.json();
        if (response.ok) {
            setSales(data.sales);
        }
    };

    useEffect(() => {
        fetchSales();
    }
    , []);

    return (
        <div className="container">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer Name</th>
                        <th>Vin</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map((sale) => (
                        <tr key={sale.id}>
                            <td>{sale.salesperson_id}</td>
                            <td>{sale.salesperson}</td>
                            <td>{sale.customer}</td>
                            <td>{sale.automobile}</td>
                            <td>{sale.price}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default ListAllSales;
