import React, { useState, useEffect } from "react";

const ListCustomers = () => {
    const [customer, setCustomer] = useState([]);

    const fetchCustomers = async () => {
        const url = "http://localhost:8090/api/customers/";
        const response = await fetch(url);
        const data = await response.json();
        if (response.ok) {
            setCustomer(data.customers);
        }
    }

    useEffect(() => {
        fetchCustomers();
    }
    , []);

    return (
        <div className="container">
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customer.map((customers) => (
                        <tr key={customers["address"]}>
                            <td>{customers.first_name}</td>
                            <td>{customers.last_name}</td>
                            <td>{customers.phone_number}</td>
                            <td>{customers.address}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default ListCustomers;
