import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import TechnicianForm from './service/TechnicianForm';
import TechniciansList from './service/TechniciansList';
import AppointmentsList from './service/AppointmentsList';
import AppointmentForm from './service/AppointmentForm';
import ServiceHistory from './service/ServiceHistory';
import ManufacturersList from './inventory/ManufacturersList';
import ManufacturerForm from './inventory/ManufacturerForm';
import ModelsList from './inventory/ModelsList';
import ModelForm from './inventory/ModelForm';
import AutomobilesList from './inventory/AutomobilesList';
import AutomobileForm from './inventory/AutomobileForm';
import SalesPersonForm from './Sales/SalesPersonForm';
import ListSalesPeople from './Sales/ListSalesPeople';
import CustomerForm from './Sales/CustomerForm';
import ListCustomers from './Sales/ListCustomers';
import SaleForm from './Sales/SaleForm';
import ListAllSales from './Sales/ListAllSales';
import SalesPersonHistory from './Sales/SalesPersonHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route index element={<MainPage />} />
          <Route path="manufacturers">
            <Route index element={<ManufacturersList />}/>
            <Route path="new" element={<ManufacturerForm />}/>
          </Route>
          <Route path="models">
            <Route index element={<ModelsList />}/>
            <Route path="new" element={<ModelForm />}/>
          </Route>
          <Route path="automobiles">
            <Route index element={<AutomobilesList />}/>
            <Route path="new" element={<AutomobileForm />}/>
          </Route>
          <Route path="technicians">
            <Route index element={<TechniciansList />}/>
            <Route path="new" element={<TechnicianForm />}/>
          </Route>
          <Route path="appointments">
            <Route index element={<AppointmentsList />}/>
            <Route path="new" element={<AppointmentForm />}/>
            <Route path="history" element={<ServiceHistory />}/>
          </Route>
          <Route path="salesperson" element={<SalesPersonForm />} />
            <Route path="salesperson/list" element={<ListSalesPeople />} />
            <Route path="salesperson/history" element={<SalesPersonHistory />} />
          <Route path="customer" element={<CustomerForm />} />
            <Route path="customer/list" element={<ListCustomers />} />
          <Route path="sale" element={<SaleForm />} />
            <Route path="sale/list" element={<ListAllSales />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
