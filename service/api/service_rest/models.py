from django.db import models

# Create your models here.

class Status(models.Model):
    name = models.CharField(max_length=10)

    def __str__(self):
        return self.name

class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100)

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=100)
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(
        Technician,
        on_delete=models.CASCADE,
    )
    status = models.ForeignKey(
        Status,
        on_delete=models.PROTECT,
        null=True,
    )

    @classmethod
    def create(cls, **kwargs):
        kwargs["status"] = Status.objects.get(name="active")
        appointment = cls(**kwargs)
        appointment.save()
        return appointment

    def cancel(self):
        status, _ = Status.objects.get_or_create(name="canceled")
        self.status = status
        self.save()

    def finish(self):
        status, _ = Status.objects.get_or_create(name="finished")
        self.status = status
        self.save()

    def __str__(self):
        return f"{self.customer} - {self.vin}"

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=100, unique=True)
